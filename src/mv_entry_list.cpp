#include <memory>
#include <shared_mutex>
#include <vector>
#include <unordered_map>
#include <iostream>
#include <mv_entry.hpp>
#include <mv_entry_list.hpp>

void mv_entry_list::add(std::shared_ptr<mv_entry> entry)
{
    std::string name = entry->name;
    if(this->list.find(name) == this->list.end()){
        this->list[name] = (entry);
    }
}

bool mv_entry_list::remove(std::string name)
{
    if( this->list.erase(name) )
        return false;
    return true;
}

bool mv_entry_list::exist(std::string name)
{
    return this->list.find(name) == this->list.end();
}

std::shared_ptr<mv_entry> mv_entry_list::get_entry(std::string name)
{
    auto it = this->list.find(name);
    if (it != this->list.end()) {
        return it->second;
    }
    return nullptr;
}