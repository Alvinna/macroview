#ifndef MV_ENTRY_MOL_H
#define MV_ENTRY_MOL_H


class mv_entry_mol : public mv_entry
{
    public:
        mv_entry_mol(std::string, OpenBabel::OBMol&);
        virtual mv_entry_type get_type() const { return MET_MOL;}
        OpenBabel::OBMol mol;
};

#endif