#include <QtCore>
#include <QtOpenGL>
#include <iostream>
#include <memory>
#include <shared_mutex>
#include <openbabel/mol.h>
#include <openbabel/atom.h>
#include <openbabel/obiter.h>
#include <json/json.h>
#include <Eigen/Dense>
#include <mv_entry.hpp>
#include <mv_entry_mol.hpp>
#include <mv_entry_list.hpp>
#include <mv_project.hpp>
#include <mv_cmd.hpp>
#include <mv_graphic.hpp>
#include <mv_scene.hpp>
#include <renderview.hpp>

renderview::renderview(mv_cmd* cmd, QObject *parent)
{
    this->cmd = cmd;
    this->scene = new mv_scene(cmd);
    this->mouse_mode = MODE_DEFAULT;
}

void renderview::initializeGL()
{
    // Initialize OpenGL
    initializeOpenGLFunctions();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT,  GL_NICEST);
    glEnable(GL_PROGRAM_POINT_SIZE);

    // Configuring shader programs and buffers

    // Line
    smgr.new_program("line");
    smgr.add_shader_file("line", QOpenGLShader::Vertex, "shaders/line.vert");
    smgr.add_shader_file("line", QOpenGLShader::Fragment, "shaders/line.frag");
    smgr.link_shader("line");
    bmgr.new_buffer("line_vert", QOpenGLBuffer::VertexBuffer);
    auto prog_line = smgr.get_program("line");
    auto buf_line = bmgr.get_buffer("line_vert");
    buf_line->setUsagePattern(QOpenGLBuffer::StaticDraw);

    //Ball
    smgr.new_program("ball");
    smgr.add_shader_file("ball", QOpenGLShader::Vertex, "shaders/ball.vert");
    smgr.add_shader_file("ball", QOpenGLShader::Fragment, "shaders/ball.frag");
    smgr.link_shader("ball");
    bmgr.new_buffer("ball_vert", QOpenGLBuffer::VertexBuffer);
    auto prog_ball = smgr.get_program("ball");
    auto buf_ball = bmgr.get_buffer("ball_vert");
    buf_ball->setUsagePattern(QOpenGLBuffer::StaticDraw);


    //prog_line->bind();
    //prog_ball->bind();

    //buf_line->bind();
    //buf_ball->bind();

    amgr.new_array("world");
    vao = amgr.get_array("world");
    //vao->bind();

    //prog_line->enableAttributeArray(0);
    //prog_line->enableAttributeArray(1);
    //prog_line->setAttributeBuffer(0, GL_FLOAT, 0, 3);
    //prog_line->setAttributeBuffer(1, GL_FLOAT, 0, 3);

    //prog_ball->enableAttributeArray(0);
    //prog_line->enableAttributeArray(1);
    //prog_ball->setAttributeBuffer(0, GL_FLOAT, 0, 3);
    //prog_line->setAttributeBuffer(1, GL_FLOAT, 0, 3);

    //vao->release();
    //buf_line->release();
    //buf_ball->release();

    //prog_line->release();
    //prog_ball->release();

    // Initialize view
    auto view = scene->get_view();
    view.translate(0, 0, -20);
    scene->set_view(view);
}

void renderview::resizeGL(int w, int h)
{
    qreal aspect = qreal(w) / qreal(h ? h : 1);
    qreal z_near, z_far, fov;
    Json::Value v;

    cmd->get_option("g_clipingPlaneNear", v);
    z_near = v.asDouble();
    cmd->get_option("g_clipingPlaneFar", v);
    z_far = v.asDouble();
    cmd->get_option("g_fov", v);
    fov = v.asDouble();

    QMatrix4x4 projection;
    projection.setToIdentity();
    projection.perspective(fov, aspect, z_near, z_far);
    scene->set_projection(projection);
}

void renderview::paintGL()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    std::vector<Eigen::Vector3f> l;
    std::vector<Eigen::Vector3f> b;

    scene->get_scene(l, b);


    auto model = scene->get_model();
    auto view = scene->get_view();
    auto projection = scene->get_projection();

    auto prog_line = smgr.get_program("line");
    auto prog_ball = smgr.get_program("ball");

    auto buf_line = bmgr.get_buffer("line_vert");
    auto buf_ball = bmgr.get_buffer("ball_vert");

    vao->bind();
    prog_line->bind();
    buf_line->bind();

    prog_line->enableAttributeArray(0);
    prog_line->setAttributeBuffer(0, GL_FLOAT, 0, 3);
    prog_line->setUniformValue("mvp_matrix", projection * view * model);
    
    buf_line->allocate(&l[0], l.size()*sizeof(Eigen::Vector3f));

    glDrawArrays(GL_LINES, 0, l.size());
    vao->release();
    buf_line->release();
    prog_line->release();


    vao->bind();
    prog_ball->bind();
    buf_ball->bind();

    prog_ball->enableAttributeArray(0);
    prog_ball->setAttributeBuffer(0, GL_FLOAT, 0, 3);
    prog_ball->setUniformValue("mvp_matrix", projection * view * model);
    
    buf_ball->allocate(&b[0], b.size()*sizeof(Eigen::Vector3f));

    glDrawArrays(GL_POINTS, 0, b.size());
    vao->release();
    buf_ball->release();
    prog_ball->release();

    /*
    vao->bind();
    prog_ball->bind();
    prog_ball->setUniformValue("mvp_matrix", projection * view * model);
    buf_ball->bind();
    buf_ball->allocate(&b[0], b.size()*sizeof(Eigen::Vector3f));
    glDrawArrays(GL_POINTS, 0, b.size());
    vao->release();
    buf_ball->release();
    prog_ball->release();   
    */
    
    
    
    
}

void renderview::update_scene()
{
    scene->update_scene();
    update();
}

void renderview::redraw()
{
    update();
}


void renderview::mousePressEvent( QMouseEvent* event )
{
    if( mouse_mode == MODE_DEFAULT ){
        if ( event->button() == Qt::LeftButton )
        {
            mouse_mode = MODE_ROTATE;
        }
        else if( event->button() == Qt::RightButton )
        {
            mouse_mode = MODE_ZOOM;
        }
        else if( event->button() == Qt::MiddleButton )
        {
            mouse_mode = MODE_MOVE;
        }
    }
    this->mouse_pos = event->pos();
}

void renderview::mouseReleaseEvent( QMouseEvent* event )
{
    if( mouse_mode == MODE_ROTATE ){
        if ( event->button() == Qt::LeftButton )
        {
            mouse_mode = MODE_DEFAULT;
        }
    }
    else if( mouse_mode == MODE_ZOOM ){
        if ( event->button() == Qt::RightButton )
        {
            mouse_mode = MODE_DEFAULT;
        }
    }
    else if( mouse_mode == MODE_MOVE ){
        if ( event->button() == Qt::MiddleButton )
        {
            mouse_mode = MODE_DEFAULT;
        }
    }
}

void renderview::mouseMoveEvent( QMouseEvent* event )
{
   
    if( mouse_mode == MODE_ROTATE ){
        auto view = scene->get_view();
        auto dx = mouse_pos.x() - event->x();
        auto dy = mouse_pos.y() - event->y();
        auto ay = view.row(1).toVector3D();
        auto ax = view.row(0).toVector3D();
        QMatrix4x4 r;
        r.setToIdentity();
        r.rotate(-0.1*dx, ay);
        r.rotate(-0.1*dy, ax);
        scene->set_view(view*r);
        update();
    }
    else if( mouse_mode == MODE_ZOOM ){
        auto view = scene->get_view();
        auto dy = mouse_pos.y() - event->y();
        auto az = view.row(2).toVector3D();
        QMatrix4x4 m;
        m.translate(0, 0, -dy * 0.01);
        scene->set_view(m*view);
        update();
        
    }
    else if( mouse_mode == MODE_MOVE ){
        auto view = scene->get_view();
        auto model = scene->get_model();
        auto dx = mouse_pos.x() - event->x();
        auto dy = mouse_pos.y() - event->y();
        auto ax = view.row(0).toVector3D();
        auto ay = view.row(1).toVector3D();
        QMatrix4x4 m;
        m.translate(-ax * 0.01 * dx + ay * 0.01 * dy);
        scene->set_model(m*model);
        update();
        
    }
    mouse_pos = event->pos();
}

#include "moc_renderview.cpp"