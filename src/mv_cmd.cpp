#include <openbabel/mol.h>
#include <openbabel/obconversion.h>
#include <unordered_map>
#include <list>
#include <shared_mutex>
#include <mv_entry.hpp>
#include <mv_entry_mol.hpp>
#include <mv_entry_list.hpp>
#include <json/json.h>
#include <QObject>
#include <mv_project.hpp>
#include <mv_cmd.hpp>

mv_cmd::mv_cmd(mv_project* project, QObject* parent)
{
    this->project = project;
}

bool mv_cmd::load_config(std::string file)
{
    auto ret = project->load_config(file);
    emit SIG_CONFIG_CHANGED();
    return ret;
}

bool mv_cmd::save_config(std::string file)
{
    return project->save_config(file);
}

bool mv_cmd::get_option(std::string option, Json::Value& result)
{
    return project->get_option(option, result);
}

void mv_cmd::set_option(std::string option, Json::Value value)
{
    project->set_option(option, value);
    emit SIG_CONFIG_CHANGED();
}

void mv_cmd::set_option(std::string option, std::string value)
{
    project->set_option(option, value);
    emit SIG_CONFIG_CHANGED();
}

void mv_cmd::set_option(std::string option, int value)
{
    project->set_option(option, value);
    emit SIG_CONFIG_CHANGED();
}

void mv_cmd::set_option(std::string option, float value)
{
    project->set_option(option, value);
    emit SIG_CONFIG_CHANGED();
}

void mv_cmd::get_entry_list(std::list<std::string>& l)
{
    project->get_entry_list(l);
}

bool mv_cmd::rename_entry(std::string oldname, std::string newname)
{
    auto ret = project->rename_entry(oldname, newname);
    emit SIG_ENTRIES_CHANGED();
    return ret;
}

bool mv_cmd::remove_entry(std::string name)
{
    auto ret = project->remove_entry(name);
    emit SIG_ENTRIES_CHANGED();
    return ret;
}

void mv_cmd::load_molfile(std::string filetype, std::string file)
{
    project->load_molfile(filetype, file);
    emit SIG_ENTRIES_CHANGED();
}

bool mv_cmd::get_entry_mol(std::string name, OpenBabel::OBMol& molecule)
{
    return project->get_entry_mol(name, molecule);
}

bool mv_cmd::set_entry_mol(std::string name, OpenBabel::OBMol& molecule)
{
    auto ret = project->set_entry_mol(name, molecule);
    emit SIG_ENTRIES_CHANGED();
    return ret;
}

bool mv_cmd::_set_entry_mol(std::string name, OpenBabel::OBMol& molecule)
{
    return project->set_entry_mol(name, molecule);
}

#include "moc_mv_cmd.cpp"