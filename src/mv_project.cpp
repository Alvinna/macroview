#include <iostream>
#include <string>
#include <fstream>
#include <memory>
#include <openbabel/mol.h>
#include <openbabel/bond.h>
#include <openbabel/atom.h>
#include <openbabel/obiter.h>
#include <openbabel/obconversion.h>
#include <openbabel/generic.h>
#include <unordered_map>
#include <shared_mutex>
#include <mv_entry.hpp>
#include <mv_entry_mol.hpp>
#include <mv_entry_list.hpp>
#include <mv_project.hpp>







bool mv_project::load_config(std::string file)
{
    std::unique_lock<std::shared_mutex> guard(config_lock);
    
    Json::CharReaderBuilder rbuilder;
    std::ifstream config_stream(file, std::ifstream::binary);
    std::string err_string;
    rbuilder["collectComments"] = false;

    if(!config_stream.is_open()){ 
        // Failed to open config file
        return false;
    }
    bool ok = Json::parseFromStream(rbuilder, config_stream, &config, &err_string);
    if ( !ok ){
        // Failed to parse config file
        return false;
    }
    return true;
}



bool mv_project::save_config(std::string file)
{
    std::shared_lock<std::shared_mutex> guard(config_lock);
    
    Json::StreamWriterBuilder wbuilder;
    std::string config_string = Json::writeString(wbuilder, config);
    std::ofstream config_stream;
    wbuilder["indentation"] = "\t";
    config_stream.open(file);
    if(!config_stream.is_open()){ 
        // Failed to open config file
        return false;
    }
    config_stream << config_string;
    config_stream.close();
    return true;
}

bool mv_project::get_option(std::string option, Json::Value& result)
{
    std::shared_lock<std::shared_mutex> guard(config_lock);
    if( !config["options"].isMember(option) )
        return false;
    result = config["options"][option];
    return true;
}

void mv_project::set_option(std::string option, Json::Value value)
{
    std::unique_lock<std::shared_mutex> guard(config_lock);
    config["options"][option] = value;
}

void mv_project::set_option(std::string option, std::string value)
{
    std::unique_lock<std::shared_mutex> guard(config_lock);
    config["options"][option] = value;
}

void mv_project::set_option(std::string option, int value)
{
    std::unique_lock<std::shared_mutex> guard(config_lock);
    config["options"][option] = value;
}

void mv_project::set_option(std::string option, float value)
{
    std::unique_lock<std::shared_mutex> guard(config_lock);
    config["options"][option] = value;
}

void mv_project::get_entry_list(std::list<std::string>& l)
{
    std::shared_lock<std::shared_mutex> guard(entry_lock);
    l.clear();
    for(auto e : entries)
    {
        l.push_back(e.first);
    }
}

bool mv_project::rename_entry(std::string oldname, std::string newname)
{
    std::unique_lock<std::shared_mutex> guard(entry_lock);
    
    if(oldname == newname)
        return true;
    if(entries.exist(oldname) && !entries.exist(newname)){
        auto e = entries.get_entry(oldname);
        e->name = newname;
        entries.add(e);
        entries.remove(oldname);  
        return true;
    }
    return false;
}

bool mv_project::remove_entry(std::string name)
{
    std::unique_lock<std::shared_mutex> guard(entry_lock);
    if( entries.remove(name) == 0)
        return false;
    return true;
}


void mv_project::load_molfile(std::string filetype, std::string file)
{
    std::unique_lock<std::shared_mutex> guard(entry_lock);

    OpenBabel::OBConversion obconversion;
    obconversion.SetInFormat(filetype.c_str());
    OpenBabel::OBMol mol;
    bool notatend = obconversion.ReadFile(&mol, file.c_str());
    while (notatend)
    {
        std::shared_ptr<mv_entry_mol> entry = std::make_shared<mv_entry_mol>(mv_entry_mol(mol.GetTitle(), mol));
        entries.add(std::dynamic_pointer_cast<mv_entry>(entry));
        mol.Clear();
        notatend = obconversion.Read(&mol);
    }
}

bool mv_project::get_entry_mol(std::string name, OpenBabel::OBMol& molecule)
{
    std::shared_lock<std::shared_mutex> guard(entry_lock);

    auto e = entries.get_entry(name);
    if(e == nullptr)
        return false;
    if(e->get_type() != MET_MOL)
        return false;
    auto m = std::dynamic_pointer_cast<mv_entry_mol>(e);
    molecule = m->mol;
    return true;
}

bool mv_project::set_entry_mol(std::string name, OpenBabel::OBMol& molecule)
{
    std::unique_lock<std::shared_mutex> guard(entry_lock);

    auto e = entries.get_entry(name);
    if(e == nullptr)
        return false;
    if(e->get_type() != MET_MOL)
        return false;
    auto m = std::dynamic_pointer_cast<mv_entry_mol>(e);
    m->mol = molecule;
    return true;
}
