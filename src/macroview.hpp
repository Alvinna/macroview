#ifndef MACROVIEW_H
#define MACROVIEW_H

class Macroview : public QMainWindow
{
    Q_OBJECT
    public:
        Macroview(QWidget *parent = nullptr);
        ~Macroview();
        mv_cmd* cmd;
        mv_project* project;

};

#endif