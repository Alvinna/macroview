#include <QMatrix4x4>
#include <shared_mutex>
#include <openbabel/mol.h>
#include <openbabel/atom.h>
#include <openbabel/obiter.h>
#include <openbabel/generic.h>
#include <json/json.h>
#include <Eigen/Dense>
#include <QtOpenGL>
#include <mv_entry.hpp>
#include <mv_entry_mol.hpp>
#include <mv_entry_list.hpp>
#include <mv_project.hpp>
#include <mv_cmd.hpp>
#include <mv_graphic.hpp>
#include <mv_scene.hpp>

mv_scene::mv_scene(mv_cmd* cmd)
{
    this->cmd = cmd;
    this->projection.setToIdentity();
    this->model.setToIdentity();
}

void mv_scene::update_scene()
{
    std::unique_lock<std::shared_mutex> guard(scene_lock);
    mol_bond.clear();
    mol_atom.clear();
    
    std::list<std::string> entries;
    cmd->get_entry_list(entries);
    for(auto entry : entries){
        OpenBabel::OBMol mol;
        auto ret = cmd->get_entry_mol(entry, mol);
        if( ret ){
            mv_style style;

            // Get/set style
            FOR_ATOMS_OF_MOL(a, mol){

                // Make sure mv_style is set
                if( a->HasData("mv_style") ){
                    auto data = dynamic_cast<OpenBabel::OBPairInteger*>(mol.GetData("mv_style"));
                    style = data->GetGenericValue();
                }
                else{
                    style = STYLE_BALLSTICK;
                    OpenBabel::OBPairInteger *data = new OpenBabel::OBPairInteger();
                    data->SetAttribute("mv_style");
                    data->SetValue(style);
                    a->SetData(data);
                }  

                // Process each style
                if( style & STYLE_BALLSTICK )
                {
                    mol_atom.push_back(Eigen::Vector3f(a->GetX(), a->GetY(), a->GetZ()));
                }
            }

            
            FOR_BONDS_OF_MOL(b, mol){
                auto a1 = b->GetBeginAtom();
                auto a2 = b->GetEndAtom();
                mol_bond.push_back(Eigen::Vector3f(a1->GetX(), a1->GetY(), a1->GetZ()));
                mol_bond.push_back(Eigen::Vector3f(a2->GetX(), a2->GetY(), a2->GetZ()));
            }
        }
    
        
        // Update style
        cmd->_set_entry_mol(entry, mol);
        
    }
}


void mv_scene::get_scene(std::vector<Eigen::Vector3f>& l, std::vector<Eigen::Vector3f>& b)
{
    std::shared_lock<std::shared_mutex> guard(scene_lock);
    l = mol_bond;
    b = mol_atom;
}

void mv_scene::set_projection(QMatrix4x4 projection)
{
    std::unique_lock<std::shared_mutex> guard(scene_lock);
    this->projection = projection;
}

QMatrix4x4 mv_scene::get_projection()
{
    std::shared_lock<std::shared_mutex> guard(scene_lock);
    return projection;
}

void mv_scene::set_model(QMatrix4x4 model)
{
    std::unique_lock<std::shared_mutex> guard(scene_lock);
    this->model = model;
}

QMatrix4x4 mv_scene::get_model()
{
    std::shared_lock<std::shared_mutex> guard(scene_lock);
    return model;
}

void mv_scene::set_view(QMatrix4x4 view)
{
    std::unique_lock<std::shared_mutex> guard(scene_lock);
    this->view = view;
}

QMatrix4x4 mv_scene::get_view()
{
    std::shared_lock<std::shared_mutex> guard(scene_lock);
    return view;
}