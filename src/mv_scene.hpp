#ifndef MV_SCENE_H
#define MV_SCENE_H

#define STYLE_LINE 0x1
#define STYLE_BALLSTICK 0x02
typedef int mv_style;

class mv_scene
{
    public:
        mv_scene(mv_cmd*);
        void update_scene();
        void get_scene(std::vector<Eigen::Vector3f>&, std::vector<Eigen::Vector3f>&);
        void set_projection(QMatrix4x4);
        QMatrix4x4 get_projection();
        void set_model(QMatrix4x4);
        QMatrix4x4 get_model();
        void set_view(QMatrix4x4);
        QMatrix4x4 get_view();

    private:
        std::shared_mutex scene_lock;
        mv_cmd* cmd;
        QMatrix4x4 projection;
        QMatrix4x4 model;
        QMatrix4x4 view;
        std::vector<Eigen::Vector3f>mol_bond;
        std::vector<Eigen::Vector3f>mol_atom;
};

#endif