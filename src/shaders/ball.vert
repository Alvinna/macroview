#version 330 core
layout(location = 0) in vec3 vertexPosition_modelspace;
  
uniform mat4 mvp_matrix;
  
void main(){
  gl_Position =  mvp_matrix * vec4(vertexPosition_modelspace,1);
  gl_PointSize = 3;
}