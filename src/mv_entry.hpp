#ifndef MV_ENTRY_H
#define MV_ENTRY_H

typedef enum {MET_NONE, MET_MOL} mv_entry_type;


class mv_entry
{
    public:
        virtual mv_entry_type get_type() const = 0;
        std::string name;
};

#endif

