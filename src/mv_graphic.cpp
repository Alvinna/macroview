#include <QtOpenGL>
#include <Eigen/Dense>
#include <unordered_map>
#include <memory>
#include <mv_graphic.hpp>

bool Graphic::shader_manager::new_program(std::string name)
{
    std::shared_ptr<QOpenGLShaderProgram> p = std::make_shared<QOpenGLShaderProgram>();
    if(programs.find(name) == programs.end()){
        programs[name] = p;
        return true;
    }
    return false;
}

bool Graphic::shader_manager::remove_program(std::string name)
{
    if( programs.erase(name) )
        return false;
    return true;
}

bool Graphic::shader_manager::add_shader_file(std::string name, QOpenGLShader::ShaderType type, std::string file)
{
   
    if( programs.find(name) != programs.end() )
    {
        auto p = programs[name];
        return p->addShaderFromSourceFile(type, QString::fromStdString(file));
    }
    return false;
}

bool Graphic::shader_manager::link_shader(std::string name)
{
   
    if( programs.find(name) != programs.end() )
    {
        auto p = programs[name];
        p->link();
        return true;
    }
    return false;
}

std::shared_ptr<QOpenGLShaderProgram> Graphic::shader_manager::get_program(std::string name)
{
    if( programs.find(name) != programs.end() )
    {
        auto p = programs[name];
        return p;
    }
    return nullptr;
}

bool Graphic::buffer_manager::new_buffer(std::string name, QOpenGLBuffer::Type type)
{
    std::shared_ptr<QOpenGLBuffer> p = std::make_shared<QOpenGLBuffer>(type);
    if(buffers.find(name) == buffers.end()){
        buffers[name] = p;
        p->create();
        return true;
    }
    return false;
}

bool Graphic::buffer_manager::remove_buffer(std::string name)
{
    if( buffers.erase(name) )
        return false;
    return true;
}

std::shared_ptr<QOpenGLBuffer> Graphic::buffer_manager::get_buffer(std::string name)
{
    if( buffers.find(name) != buffers.end() )
    {
        auto p = buffers[name];
        return p;
    }
    return nullptr;
}

bool Graphic::array_manager::new_array(std::string name)
{
    std::shared_ptr<QOpenGLVertexArrayObject> p = std::make_shared<QOpenGLVertexArrayObject>();
    if(arrays.find(name) == arrays.end()){
        arrays[name] = p;
        p->create();
        return true;
    }
    return false;
}

bool Graphic::array_manager::remove_array(std::string name)
{
    if( arrays.erase(name) )
        return false;
    return true;
}

std::shared_ptr<QOpenGLVertexArrayObject> Graphic::array_manager::get_array(std::string name)
{
    if( arrays.find(name) != arrays.end() )
    {
        auto p = arrays[name];
        return p;
    }
    return nullptr;
}