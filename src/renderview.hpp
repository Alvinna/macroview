#ifndef RENDERVIEW_H
#define RENDERVIEW_H

typedef enum
{
    MODE_DEFAULT,
    MODE_ROTATE,
    MODE_ZOOM,
    MODE_MOVE
}mmode;

class renderview : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
    public:
        explicit renderview(mv_cmd* cmd, QObject *parent);
        virtual void mouseMoveEvent(QMouseEvent *event);
        virtual void mousePressEvent(QMouseEvent *event);
        virtual void mouseReleaseEvent(QMouseEvent *event);
    public slots:    
        void initializeGL() override;
        void resizeGL(int w, int h) override;
        void paintGL() override;
        void update_scene();

    private:
        mv_cmd *cmd;
        mv_scene *scene;

        Graphic::shader_manager smgr;
        Graphic::buffer_manager bmgr;
        Graphic::array_manager amgr;
        std::shared_ptr<QOpenGLVertexArrayObject> vao;
        mmode mouse_mode;
        QPoint mouse_pos;
        
    public slots:
        void redraw(void);

};

#endif