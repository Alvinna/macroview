#ifndef MV_GRAPHIC_H
#define MV_GRAPHIC_H

namespace Graphic
{
    class shader_manager
    {
        public:
            bool new_program(std::string);
            bool remove_program(std::string);
            bool add_shader_file(std::string, QOpenGLShader::ShaderType, std::string);
            bool link_shader(std::string);
            std::shared_ptr<QOpenGLShaderProgram> get_program(std::string);
        private:
            std::unordered_map<std::string, std::shared_ptr<QOpenGLShaderProgram>> programs;
    };

    class buffer_manager
    {
        public:
            bool new_buffer(std::string, QOpenGLBuffer::Type);
            bool remove_buffer(std::string);
            std::shared_ptr<QOpenGLBuffer> get_buffer(std::string);
        private:
            std::unordered_map<std::string, std::shared_ptr<QOpenGLBuffer>> buffers;
    };

    class array_manager
    {
        public:
            bool new_array(std::string);
            bool remove_array(std::string);
            std::shared_ptr<QOpenGLVertexArrayObject> get_array(std::string);
        private:
            std::unordered_map<std::string, std::shared_ptr<QOpenGLVertexArrayObject>> arrays;
    };
};


#endif