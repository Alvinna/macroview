//mv_cmd is a class wrapper to mv_project, which might change argument slightly or add qt signal
#ifndef MV_CMD_H
#define MV_CMD_H

class mv_cmd : public QObject
{    
    Q_OBJECT
    public:

        mv_cmd(mv_project*, QObject* parent);
        mv_project* get_project();

        // Config related
        bool load_config(std::string file);
        bool save_config(std::string file);
        bool get_option(std::string, Json::Value&);
        void set_option(std::string, Json::Value);
        void set_option(std::string, std::string);
        void set_option(std::string, int);
        void set_option(std::string, float);

        //Entry related
        void get_entry_list(std::list<std::string>&);
        bool rename_entry(std::string, std::string);
        bool remove_entry(std::string);

        //Derived entry specific
        void load_molfile(std::string, std::string);
        bool get_entry_mol(std::string, OpenBabel::OBMol&);
        bool set_entry_mol(std::string, OpenBabel::OBMol&);

        //Internal( avoid signalling)
        bool _set_entry_mol(std::string, OpenBabel::OBMol&);

    private:
        mv_project* project;
    signals:
        void SIG_CONFIG_CHANGED(void);
        void SIG_ENTRIES_CHANGED(void);

};

#endif