#ifndef MV_ENTRY_LIST_H
#define MV_ENTRY_LIST_H

class mv_entry_list
{
    public:

        void add(std::shared_ptr<mv_entry>);
        bool remove(std::string);
        bool exist(std::string);
        std::shared_ptr<mv_entry> get_entry(std::string);

        using iterator = std::unordered_map<std::string, std::shared_ptr<mv_entry>>::iterator;
        using const_iterator = std::unordered_map<std::string, std::shared_ptr<mv_entry>>::const_iterator;
        iterator begin() { return list.begin(); }
        iterator end() { return list.end(); }
        const_iterator begin() const { return list.begin(); }
        const_iterator end() const { return list.end(); }
        const_iterator cbegin() const { return list.cbegin(); }
        const_iterator cend() const { return list.cend(); }
    private:
    
        std::unordered_map<std::string, std::shared_ptr<mv_entry>> list;

};

#endif