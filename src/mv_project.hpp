#ifndef MV_PROJECT_H
#define MV_PROJECT_H

#include <unordered_map>
#include <list>
#include <openbabel/mol.h>
#include <openbabel/bond.h>
#include <openbabel/atom.h>
#include <openbabel/obiter.h>
#include <openbabel/generic.h>
#include <shared_mutex>
#include <mv_entry.hpp>
#include <mv_entry_mol.hpp>
#include <mv_entry_list.hpp>
#include <json/json.h>




class mv_project
{
    public:

        // Config related
        bool load_config(std::string file);
        bool save_config(std::string file);
        bool get_option(std::string, Json::Value&);
        void set_option(std::string, Json::Value);
        void set_option(std::string, std::string);
        void set_option(std::string, int);
        void set_option(std::string, float);

        //Entry related
        void get_entry_list(std::list<std::string>&);
        bool rename_entry(std::string, std::string);
        bool remove_entry(std::string);

        //Derived entry specific
        void load_molfile(std::string, std::string);
        bool get_entry_mol(std::string, OpenBabel::OBMol&);
        bool set_entry_mol(std::string, OpenBabel::OBMol&);


    private:
        std::shared_mutex config_lock;
        std::shared_mutex entry_lock;
        Json::Value config;
        mv_entry_list entries;

};
#endif