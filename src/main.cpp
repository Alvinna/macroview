#include <iostream>
#include <string>
#include <fstream>
#include <QtCore>
#include <QApplication>
#include <QtWidgets>
#include <QMainWindow>
#include <QtOpenGL>
#include <json/json.h>
#include <Eigen/Dense>
#include <mv_project.hpp>
#include <mv_cmd.hpp>
#include <mv_graphic.hpp>
#include <mv_scene.hpp>
#include <renderview.hpp>
#include <macroview.hpp>

int main(int argc, char **argv)
{
    QApplication macroview_app (argc, argv);
    Macroview macroview;
    macroview.resize(QSize(800, 800));
    

    return macroview_app.exec();
}