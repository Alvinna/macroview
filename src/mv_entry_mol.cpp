#include <openbabel/mol.h>
#include <openbabel/generic.h>
#include <openbabel/atom.h>
#include <openbabel/obiter.h>
#include <memory>
#include <shared_mutex>
#include <mv_entry.hpp>
#include <mv_entry_mol.hpp>

mv_entry_mol::mv_entry_mol(std::string name, OpenBabel::OBMol &molecule)
{
    this->name = name;
    this->mol = molecule;

}
