#include <iostream>
#include <string>
#include <fstream>
#include <memory>
#include <QtCore>
#include <QMainWindow>
#include <QtWidgets>
#include <QGridLayout>
#include <QtOpenGL>
#include <json/json.h>
#include <Eigen/Dense>
#include <shared_mutex>
#include <openbabel/atom.h>
#include <openbabel/obiter.h>
#include <openbabel/generic.h>
#include <mv_project.hpp>
#include <mv_cmd.hpp>
#include <mv_graphic.hpp>
#include <mv_scene.hpp>
#include <renderview.hpp>
#include <macroview.hpp>

Macroview::Macroview(QWidget *parent)
{
    // Resources Allocation
    this->project = new mv_project();
    this->cmd = new mv_cmd(this->project, this);
    // Main 
    cmd->load_config("default_config.json");
    // configure opengl
    QSurfaceFormat format;
    Json::Value t;
    int dbuf_size, sbuf_size;
    int gl_version_maj, gl_version_min;
    std::string gl_profile;
    cmd->get_option("g_depthBufferSizw", t);
    dbuf_size = t.asInt();
    cmd->get_option("g_stencilBufferSizw", t);
    sbuf_size = t.asInt();
    cmd->get_option("g_openglVersion", t);
    std::stringstream ss(t.asString());
    std::string vstr;
    getline(ss, vstr, '.');
    gl_version_maj = std::stoi(vstr);
    getline(ss, vstr, '.');
    gl_version_min = std::stoi(vstr);
    cmd->get_option("g_openglProfile", t);
    gl_profile = t.asString();

    format.setDepthBufferSize(dbuf_size);
    format.setStencilBufferSize(sbuf_size);
    format.setVersion(gl_version_maj, gl_version_min);
    if( gl_profile == "core" )
        format.setProfile(QSurfaceFormat::CoreProfile);
    else if( gl_profile == "compat")
        format.setProfile(QSurfaceFormat::CompatibilityProfile);
    else
        format.setProfile(QSurfaceFormat::NoProfile);
    format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);

    //Widget
    QWidget *frame = new QWidget();
    this->setCentralWidget(frame);
    QGridLayout *layout = new QGridLayout;
    frame->setLayout(layout);

    renderview *view = new renderview(cmd, this);
    connect(cmd, SIGNAL(SIG_ENTRIES_CHANGED()), view, SLOT(update_scene()));
    layout->addWidget(view);
    
    this->show();

    // Test
    cmd->load_molfile("pdb", "r.pdb");

}

Macroview::~Macroview()
{
    delete this->cmd;
    delete this->project;
}

#include "moc_macroview.cpp"